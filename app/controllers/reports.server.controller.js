'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    _ = require('lodash'),
	Timesheet = mongoose.model('Timesheet');


/**
 * Create a Report
 */
exports.timesheets = function(req, res) {
	var params = {};
	if(req.body.user){
		params.user = req.body.user;
	}
	if(req.body.project){
		params.project = req.body.project;
	}
	if(req.body.startDate){
		req.body.startDate = req.body.startDate.substr(0,10);
		params.end = { $gte: req.body.startDate };
	}
	if(req.body.endDate){
		req.body.endDate = req.body.endDate.substr(0,10);
		params.start = { $lte: req.body.endDate };
	}
	Timesheet.find(params)
		//.populate('user', 'displayName')
		//.populate('project','name')
		.sort('user')
		.lean(true)
		.exec(function(err,timesheets){
			res.jsonp(timesheets);
	});
};

/**
 * Show the current Report
 */
exports.read = function(req, res) {

};

/**
 * Update a Report
 */
exports.update = function(req, res) {

};

/**
 * Delete an Report
 */
exports.delete = function(req, res) {

};

/**
 * List of Reports
 */
exports.list = function(req, res) {

};