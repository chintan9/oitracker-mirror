'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;
var TrimmedString = {type:String, trim:true};

/**
 * Invoice Schema
 */
var InvoiceSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Invoice name',
		trim: true
	},
	project: {
		type: Schema.ObjectId,
		ref: 'Project',
		required: 'Please select a project'
	},
	consultantName: TrimmedString,
	billTo:TrimmedString,
	fromDate:Date,
	toDate:Date,
	invoiceAddress: TrimmedString,
	invoiceDate:Date,
	dueDate: Date,
	client:TrimmedString,
	location: TrimmedString,
	items : [{
		_id:false,
		description: TrimmedString,
		rate : TrimmedString,
		hours: TrimmedString,
		total:TrimmedString
	}],
	total:Number,
	status:[{
		status:TrimmedString,
		effectiveDate: {
			_id:false,
			type: Date,
			default: Date.now
		},
		user:TrimmedString
	}],
	sentTo:TrimmedString,
	sent: {
		_id:false,
		type: Date,
		default: Date.now
	},
	lastInvoiceDate:Date
});

mongoose.model('Invoice', InvoiceSchema);