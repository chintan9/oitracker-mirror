'use strict';
// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
var _ = require('lodash');
var fs = require('fs');
var async = require('async');
var uploadPath = 'uploads/';
var config = require('../../config/config');
var bucket = config.aws.bucket;

/**
 * Don't hard-code your credentials!
 * Export the following environment variables instead:
 *
 * export AWS_ACCESS_KEY_ID='AKID'
 * export AWS_SECRET_ACCESS_KEY='SECRET'
 */

AWS.config.update(config.aws.config);
var s3 = new AWS.S3();
/*
Load files to AWS S3
params = { key:'KEY', file:'FilePath' }
*/
var uploadFile = function(params,cbfn){
	var obj = {
		Bucket: bucket,
		Key: params.key,
		Body: fs.readFileSync(params.file)
	};
	s3.putObject(obj ,cbfn);
};

var getFileName = function(doc){
	return doc.type.replace(/[^a-z0-9_\.\-]/gi, '_') + '-'+ (new Date().getTime()) +'.' + doc.extension;
};

exports.uploadFile = uploadFile;

/**
 * Get the error message from error object
 */
var getErrorMessage = function(err) {
	var message = '';

	if (err.code) {
		switch (err.code) {
			case 11000:
			case 11001:
				message = 'File already exists';
				break;
			default:
				message = 'Something went wrong';
		}
	} else {
		for (var errName in err.errors) {
			if (err.errors[errName].message) message = err.errors[errName].message;
		}
	}

	return message;
};

exports.upload = function(Model){
	return function(req,res){
		var docs = [];
		req.body.fileNames = req.body.fileNames.split('/\\');
		req.body.fileNames.forEach(function(e,i){
			docs.push({
				filePath:(req.files.file.path || req.files.file[i].path),
				type:e,
				mime:(req.files.file.mimetype || req.files.file[i].mimetype),
				extension:(req.files.file.extension || req.files.file[i].extension),
				noDelete: !!_.intersection(req.user.roles, ['admin']).length
			});
		});
		async.map(docs, function(doc,cbfn){
			doc.file = getFileName(doc);
			var obj = {
				Bucket: bucket,
				Key: Model.modelName + '/' + (doc.file.replace(uploadPath,'')),
				Body: fs.readFileSync(doc.filePath),
		//		ContentDisposition:'attachment; filename=' + doc.file,
				ContentType:doc.mime
			};
			s3.putObject(obj,cbfn);
		},

		function(err, results){
			var unlinkcb = function(e){console.log('unlink error');};
			for(var i=0;i<results.length;i++){
				if(!results[i] || !results[i].ETag){
					docs[i].s3error = true;
					//TODO: send email with file attachment
				} else {
					fs.unlink(docs[i].file,unlinkcb);
				}
			}

			Model.findById(req.body._id).exec(function(err,model){
				if (err) {
					var deleteObj = [];
					for(var i=0;i<docs.length;i++){
						deleteObj.push({key:Model.modelName + '/' + (docs[i].file.replace(uploadPath,''))});
					}
					var params = {
						Bucket: bucket,
						Delete: {
							Objects: deleteObj,
							Quiet: true
						}
					};
					s3.deleteObjects(params,function(err){
						if(err){ err=0;}//TODO: send email about error with key
					});
					return res.send(400, {
						message: getErrorMessage(err)
					});
				} else {
					model.docs = model.docs.concat(docs);
					model.save(function(err) {
						if (err) {
							return res.send(400, {
								message: getErrorMessage(err)
							});
						} else {
							// Remove unwanted fields;
							model.password = undefined;
							model.salt = undefined;
							model.roles = undefined;
							res.jsonp(model);
						}
					});
				}
			});
		});
	};
};

exports.deleteDoc = function(Model){
	return function(req,res){

		var documentSearch = { file: req.body.file, type: req.body.type, noDelete: false};
		if(_.intersection(req.user.roles, ['admin']).length){
			delete documentSearch.noDelete;
		}

		Model.findOneAndUpdate(
			{ _id: req.body._id },
			{ $pull: { 'docs': documentSearch}},
			{ multi: false },
			function(err,result){
				if (err) {
					return res.send(400, {
						message: getErrorMessage(err)
					});
				} else {
					s3.deleteObject({Bucket: bucket,Key: Model.modelName + '/' + req.body.file.replace(uploadPath,'') },function(err){
						if(err){ err=0;}//TODO: send email about error with key
					});
					// Remove unwanted fields;
					result.password = undefined;
					result.salt = undefined;
					result.roles = undefined;
					res.jsonp(result);
				}
			}
		);
	};
};

exports.download = function(Model){
	return function(req,res){
		var modelId = req.params.modelId;
		var key = req.params[0];
		var params = {Bucket: bucket, Key: Model.modelName + '/' + (key.replace(uploadPath,''))};
		var url = s3.getSignedUrl('getObject', params);
		res.redirect(url);
	};
};
