'use strict';
/**
 * Module dependencies.
 */
var should = require('should'),
	s3upload = require('../modules/s3upload');

/**
 * Unit tests
 */
describe('Upload File Unit Test:', function() {

	describe('Upload File', function() {
		it('should be able to save without problems', function(done) {
			s3upload.uploadFile({file:'/Users/crazy/Downloads/DOMexe.html', key:'Users/crazy/Downloads/DOMexe.html'},function(err,data){
				should.not.exist(err);
				done();
			});
		});
	});
});