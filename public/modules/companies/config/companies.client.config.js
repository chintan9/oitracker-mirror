'use strict';

// Configuring the Articles module
angular.module('companies').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Companies', 'companies', 'dropdown', '/companies(/create)?',false,['admin']);
		Menus.addSubMenuItem('topbar', 'companies', 'View Companies', 'companies');
		Menus.addSubMenuItem('topbar', 'companies', 'Reports', 'reports');
	}
]);