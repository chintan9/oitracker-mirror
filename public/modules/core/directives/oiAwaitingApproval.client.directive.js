'use strict';
angular.module('core').directive('oiAwaitingApproval',function(){
	return {
		restrict: 'E',
		templateUrl:'modules/users/views/oi-awaitingApproval.html'
	};
});