/*globals _*/
'use strict';
angular.module('core').directive('oiDocuments',function(){
		return {
			restrict: 'EA',
			templateUrl: 'modules/core/utils/documents.html',
			scope: {
				docs: '=docs',
				functions:'=?',
				model:'=?',
				nodelete:'=?',
				noupload:'=?',
				urlbase:'@'
			},
			controller: ['$scope','$modal','$http','$upload','coreConfig',function($scope,$modal,$http,$upload,coreConfig){

				if(coreConfig.user && _.intersection(coreConfig.user.roles, ['admin']).length){
					$scope.isAdmin = true;
				}

				$scope.files = {files:[],fileNames:[]};
				$scope.onFileSelect = function(files){
					$scope.files.files = $scope.files.files.concat(files);
					angular.forEach(files,function(val,key){
						$scope.files.fileNames.push(val.name.substr(0,val.name.lastIndexOf('.')));
					});
					$scope.$apply();
				};

				$scope.upload = function(files){
					$upload.upload({
						url:$scope.urlbase+'/upload',
						data: {_id: $scope.model._id, fileNames: files.fileNames.join('/\\')},
						file: files.files
					}).success(function(data, status, headers, config) {
						$scope.model.docs = data.docs;
						files.fileNames = [];
						files.files = [];
					});
				};
				$scope.deleteDoc = function(obj){
					var data = angular.copy(obj);
					data._id = $scope.model._id;
					$http.post($scope.urlbase+'/deleteDoc', data).
						success(function(data, status, headers, config) {
							$scope.model.docs = data.docs;
						}).
						error(function(data, status, headers, config) {
							alert('Error while deleting document');
						});
				};
				if($scope.functions){
					if($scope.functions.upload){
						$scope.upload = $scope.functions.upload;
					}
					if($scope.functions.deleteDoc){
						$scope.deleteDoc = $scope.functions.deleteDoc;
					}
				}

				$scope.preDeleteDoc = function(obj){
					if(prompt('Please enter word "DELETE" to delete this document').toLowerCase()==='delete') {
						$scope.deleteDoc(obj);
					}
					/*
					 var $childScope = $scope.$new(true);
					 $childScope.body = 'Do you want to delete this file?';
					 var modalInstance=$modal.open({
					 templateUrl:'modules/core/utils/confirmDialog.html',
					 scope:$childScope
					 });
					 modalInstance.result.then(function (status) {
					 $childScope.$destroy();
					 $scope.deleteDoc(obj);
					 }, function (reason) {
					 $childScope.$destroy();
					 });*/
				};

				$scope.removeFile = function(ind){
					$scope.files.files.splice(ind,1);
					$scope.files.fileNames.splice(ind,1);
				};

				$scope.openDoc = function(evt){
					var href = 'a.dummy'; // Disabled modal dialog for images. 
					//var href = evt.target.href;
					switch(href.split('.').pop().toLowerCase()){
						case 'jpeg':
						case 'jpg':
						case 'gif':
						case 'png':
							evt.preventDefault();
							var modalInstance=$modal.open({
								template:'<img src="'+ href +'" style="width:100%" />',
								size: 'lg'
							});
					}
				};
			}]
		};
	});